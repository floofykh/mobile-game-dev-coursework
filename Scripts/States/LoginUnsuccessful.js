/**
 * Created by Christie on 16/03/2015.
 */

Game.LoginUnsuccessful = function(game){};
Game.LoginUnsuccessful.prototype = {

    /**
     * Display images
     * Add the buttons that will start the game or re-handle login
     */
    create: function(){
        this.add.sprite(0, 0, 'background');
        this.add.button(Game.GAME_WIDTH-350, Game.GAME_HEIGHT-150, 'mainMenu', this.mainMenu, this, 1, 0, 2);
        this.add.button(Game.GAME_WIDTH-700, Game.GAME_HEIGHT-150, 'login', this.login, this, 1, 0, 2);
    },

    /**
     * Start the Main Menu state
     */
    mainMenu: function() {
        this.state.start('MainMenu');
    },

    /**
     * Go to Login Screen
     */
    login: function() {
        this.state.start('Login');
    }
};