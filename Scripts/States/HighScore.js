/**
 * Created by Christie on 16/03/2015.
 */

Game.HighScore = function(game){
    /**
     *
     * @type {Array} - Contains objects of type {name:{string}, score{number}}
     */
    this.highscores = [
       /* {name: 'rawr', score: 100},
        {name: 'grrr', score: 333},
        {name: 'AWD', score: 212},
        {name: 'dawd', score: 24},
        {name: 'sdfs', score: 5454},
        {name: 'frgr', score: 5},
        {name: 'dfd', score: 12},
        {name: '33', score: 8776},
        {name: 'dawd3fd', score: 87},
        {name: 'zxzx', score: 6300}*/
    ];
};
Game.HighScore.prototype = {
    /**
     *  Displays background and buttons
     *  Checks if music is playable and if it is then plays it
     *  Sets up and displays high scores
     */
    create: function(){
        if(Game.loggedIn){
            var _this = this;
            Game.socket.on('getHighscoresFailed', this.loadError);
            Game.socket.on('getHighscores', function(data){
                var namex = 250, scorex = 450, y = 100;

                var highScores = [];
                var numHighscoresInserted = 0;
                var score = null, name = null;

                //Copy 10 highest scores to array
                for(var i = 0; i < data.Items.length; i++)
                {
                    score = data.Items[i].Score.N;
                    name = data.Items[i].PlayerName.S;

                    if(highScores.length == 0){
                        highScores[0] = {name: name, score: score};
                        numHighscoresInserted++;
                    }
                    else{
                        for(var j=0; j<=numHighscoresInserted; j++){
                            if(j == numHighscoresInserted){
                                if(numHighscoresInserted <= 10){
                                    highScores[j] = {name: name, score: score};
                                    numHighscoresInserted++;
                                    break;
                                }
                            }
                            else if(parseInt(highScores[j].score) < score){
                                //Move element up and insert new score
                                var temp = {name: name, score: score};
                                for(var k=j; k<=numHighscoresInserted; k++){
                                    if(k <= 10 && k< numHighscoresInserted){
                                        var newTemp = highScores[k];
                                        highScores[k] = temp;
                                        temp = newTemp;
                                    }
                                    else{
                                        highScores[k] = temp;
                                    }
                                }
                                numHighscoresInserted++;
                                break;
                            }
                        }
                    }
                }

                for(var i = 0, len = data.Count; i < len; i++)
                {
                    var text = _this.add.text(namex, y, highScores[i].name, { fontSize: '28px',  fill: '#ffffff' });
                    var text2 = _this.add.text(scorex, y, highScores[i].score, { fontSize: '28px',  fill: '#ffffff' });
                    y += 35;
                }
            });
            Game.socket.emit('getHighscores');
        }

        this.add.sprite(0, 0, 'background');
        this.highscoremusic = this.add.audio('highscore');
        if(Game.musicPlayable == true)
        {
            this.highscoremusic.play("", 0, 1, true, true);
        }

        mainmenubutton = this.add.button(Game.GAME_WIDTH - 525, Game.GAME_HEIGHT - 100, 'mainMenu', this.mainMenu, this);
        mainmenubutton.onInputOver.add(this.mainMenuOver, this);
        mainmenubutton.onInputOut.add(this.mainMenuOut, this);
        mainmenubutton.onInputDown.add(this.mainMenuDown, this);
        mainmenubutton.onInputUp.add(this.mainMenuUp, this);

        var highscore = this.add.text(240, 10, 'HIGHSCORES', { fontSize: '48px',  fill: '#ffffff' });
    },

    /**
     * Takes user to main menu
     */
    mainMenu: function() {
        //Go to Options Screen
        this.highscoremusic.stop();
        this.state.start('MainMenu');
    },

    /**
     * Change button image in correlation to state
     */
    mainMenuOver: function () {
        mainmenubutton.setTexture(PIXI.TextureCache['mainMenuHover']);
    },
    /**
     * Change button image in correlation to state
     */
    mainMenuOut: function () {
        mainmenubutton.setTexture(PIXI.TextureCache['mainMenu']);
    },
    /**
     * Change button image in correlation to state
     */
    mainMenuDown: function () {
        mainmenubutton.setTexture(PIXI.TextureCache['mainMenuPressed']);
    },
    /**
     * Change button image in correlation to state
     */
    mainMenuUp: function () {
        mainmenubutton.setTexture(PIXI.TextureCache['mainMenu']);
    },

    loadError: function(){

    }
};