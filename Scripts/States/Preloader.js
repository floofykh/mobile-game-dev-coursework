/**
 * Created by Christie on 14/03/2015.
 */

/**
 * Define width and height of the game
 * Game.musicPlayable; //Bool for music on and off
 * Game.soundPlayable = true; //Bool for sound on and off
 * @param game
 * @constructor
 */
Game.Preloader = function(game){
    Game.GAME_WIDTH = game.width;
    Game.GAME_HEIGHT = game.height;
    Game.musicPlayable = true;
    Game.soundPlayable = true;
};
Game.Preloader.prototype = {
    preload: function(){
        /**
         * set background color and preload image bar
         * @type {string}
         */
        this.stage.backgroundColor = '#000000';
        this.preloadBar = this.add.sprite((Game.GAME_WIDTH-311)/2, (Game.GAME_HEIGHT-27)/2, 'preloaderBar');
        var dungeon = this.add.text(150, 80, 'DUNGEON CRAWLER', { fontSize: '48px',  fill: '#ffffff' });
        this.load.setPreloadSprite(this.preloadBar);
        /**
         * load images for game
         */
        this.load.image('background', 'Images/bloodywall/ZombieInvasionBg1.png');
        this.load.image('floor', 'Images/platform.png');
        
        //Load the floors
        this.load.image('floor1', 'Images/Floors/floor1.png');
        this.load.image('floor2', 'Images/Floors/floor2.png');
        this.load.image('floor3', 'Images/Floors/floor3.png');
        this.load.image('floor4', 'Images/Floors/floor4.png');
        this.load.image('floor5', 'Images/Floors/floor5.png');
        this.load.image('floor6', 'Images/Floors/floor6.png');
        this.load.image('floor7', 'Images/Floors/floor7.png');
        this.load.image('floor8', 'Images/Floors/floor8.png');
        this.load.image('floor9', 'Images/Floors/floor9.png');
        this.load.image('floor10', 'Images/Floors/floor10.png');
        this.load.image('floor11', 'Images/Floors/floor11.png');
        this.load.image('floor12', 'Images/Floors/floor12.png');
        //Load the walls
        this.load.image('wall1', 'Images/Walls/wall1.png');
        this.load.image('wall2', 'Images/Walls/wall2.png');
        this.load.image('wall3', 'Images/Walls/wall3.png');
        this.load.image('wall4', 'Images/Walls/wall4.png');
        this.load.image('wall5', 'Images/Walls/wall5.png');
        this.load.image('wall6', 'Images/Walls/wall6.png');
        this.load.image('wall7', 'Images/Walls/wall7.png');
        this.load.image('wall8', 'Images/Walls/wall8.png');
        this.load.image('wall9', 'Images/Walls/wall9.png');
        this.load.image('wall10', 'Images/Walls/wall10.png');
        this.load.image('wall11', 'Images/Walls/wall11.png');
        this.load.image('wall12', 'Images/Walls/wall12.png');

        this.load.image('pause', 'Images/Buttons/Pause.png');
        this.load.image('pauseHover', 'Images/Buttons/PauseHover.png');
        this.load.image('pausePressed', 'Images/Buttons/PausePressed.png');

        this.load.image('play', 'Images/Buttons/Play.png');
        this.load.image('playHover', 'Images/Buttons/PlayHover.png');
        this.load.image('playPressed', 'Images/Buttons/PlayPressed.png');

        this.load.image('login', 'Images/Buttons/Login.png');
        this.load.image('loginHover', 'Images/Buttons/LoginHover.png');
        this.load.image('loginPressed', 'Images/Buttons/LoginPressed.png');

        this.load.image('mainMenu', 'Images/Buttons/MainMenu.png');
        this.load.image('mainMenuHover', 'Images/Buttons/MainMenuHover.png');
        this.load.image('mainMenuPressed', 'Images/Buttons/MainMenuPressed.png');

        this.load.image('options', 'Images/Buttons/Options.png');
        this.load.image('optionsHover', 'Images/Buttons/OptionsHover.png');
        this.load.image('optionsPressed', 'Images/Buttons/OptionsPressed.png');

        this.load.image('restart', 'Images/Buttons/Restart.png');
        this.load.image('restartHover', 'Images/Buttons/RestartHover.png');
        this.load.image('restartPressed', 'Images/Buttons/RestartPressed.png');

        this.load.image('nextLevel', 'Images/Buttons/NextLevel.png');
        this.load.image('nextLevelHover', 'Images/Buttons/NextLevelHover.png');
        this.load.image('nextLevelPressed', 'Images/Buttons/NextLevelPressed.png');

        this.load.image('next', 'Images/Buttons/Next.png');
        this.load.image('nextHover', 'Images/Buttons/NextHover.png');
        this.load.image('nextPressed', 'Images/Buttons/NextPressed.png');

        this.load.image('previous', 'Images/Buttons/Previous.png');
        this.load.image('previousHover', 'Images/Buttons/PreviousHover.png');
        this.load.image('previousPressed', 'Images/Buttons/PreviousPressed.png');

        this.load.image('musicON', 'Images/Buttons/Music.png');
        this.load.image('musicONHover', 'Images/Buttons/MusicHover.png');
        this.load.image('musicONPressed', 'Images/Buttons/MusicPressed.png');

        this.load.image('musicOFF', 'Images/Buttons/NoMusic.png');
        this.load.image('musicOFFHover', 'Images/Buttons/NoMusicHover.png');
        this.load.image('musicOFFPressed', 'Images/Buttons/NoMusicPressed.png');

        this.load.image('soundsON', 'Images/Buttons/Sounds.png');
        this.load.image('soundsONHover', 'Images/Buttons/soundsHover.png');
        this.load.image('soundsONPressed', 'Images/Buttons/SoundsPressed.png');

        this.load.image('soundsOFF', 'Images/Buttons/NoSounds.png');
        this.load.image('soundsOFFHover', 'Images/Buttons/NoSoundHover.png');
        this.load.image('soundsOFFPressed', 'Images/Buttons/NoSoundPressed.png');

        this.load.image('highscores', 'Images/Buttons/HighScores.png');
        this.load.image('highscoresHover', 'Images/Buttons/HighScoresHover.png');
        this.load.image('highscoresPressed', 'Images/Buttons/HighScoresPressed.png');

        this.load.image('star', 'Images/star.png');
        this.load.image('goal','Images/yellow_ball.png');
        this.load.image('bullet', 'Images/bullet2.png');
        this.load.image('enemyBullet', 'Images/bullet2.png');
        this.load.spritesheet('player', 'Images/Player/Player.png', 16, 16, 16);
        this.load.spritesheet('spider', 'Images/character/spider/spider idle.png', 32, 16);

        this.load.audio('collect', 'Sounds/collect.mp3');
        this.load.audio('game', 'Sounds/game.wav');
        this.load.audio('highscore', 'Sounds/highscore.mp3');
        this.load.audio('mainmenu', 'Sounds/MainMenu.mp3');
        this.load.audio('levelcomplete', 'Sounds/levelcomplete.mp3');
    },

    /**
     * Start the MainMenu state
     */
    create: function(){
        this.state.start('MainMenu');
    }
};

