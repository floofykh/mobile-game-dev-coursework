/**
 * Created by Christie on 16/03/2015.
 */

Game.Login = function(game){};
Game.Login.prototype = {

    /**
     * Display images
     * Add the buttons that will take the user to the login successful screen or main menu
     */
    create: function(){
        this.add.sprite(0, 0, 'background');

        loginbutton = this.add.button(Game.GAME_WIDTH - 525, Game.GAME_HEIGHT - 400, 'login', this.login, this);
        loginbutton.onInputOver.add(this.loginOver, this);
        loginbutton.onInputOut.add(this.loginOut, this);
        loginbutton.onInputDown.add(this.loginDown, this);
        loginbutton.onInputUp.add(this.loginUp, this);

        mainmenubutton = this.add.button(Game.GAME_WIDTH - 525, Game.GAME_HEIGHT - 200, 'mainMenu', this.mainMenu, this);
        mainmenubutton.onInputOver.add(this.mainMenuOver, this);
        mainmenubutton.onInputOut.add(this.mainMenuOut, this);
        mainmenubutton.onInputDown.add(this.mainMenuDown, this);
        mainmenubutton.onInputUp.add(this.mainMenuUp, this);

        var login = this.add.text(320, 40, 'LOGIN', { fontSize: '48px',  fill: '#ffffff' });
    },

    /**
     * Take user to login successful screen
     */
    login: function() {
        Game.playerName = prompt("Please enter your username: ");
        this.state.start('LoginSuccessful');
    },

    /**
     * Go to Main Menu Screen
     */
    mainMenu: function() {
        this.state.start('MainMenu');
    },

    /**
     * Change button image in correlation to state
     */
    mainMenuOver: function () {
        mainmenubutton.setTexture(PIXI.TextureCache['mainMenuHover']);
    },
    /**
     * Change button image in correlation to state
     */
    mainMenuOut: function () {
        mainmenubutton.setTexture(PIXI.TextureCache['mainMenu']);
    },
    /**
     * Change button image in correlation to state
     */
    mainMenuDown: function () {
        mainmenubutton.setTexture(PIXI.TextureCache['mainMenuPressed']);
    },
    /**
     * Change button image in correlation to state
     */
    mainMenuUp: function () {
        mainmenubutton.setTexture(PIXI.TextureCache['mainMenu']);
    },

    /**
     * Change button image in correlation to state
     */
    loginOver: function () {
        loginbutton.setTexture(PIXI.TextureCache['loginHover']);
    },
    /**
     * Change button image in correlation to state
     */
    loginOut: function () {
        loginbutton.setTexture(PIXI.TextureCache['login']);
    },
    /**
     * Change button image in correlation to state
     */
    loginDown: function () {
        loginbutton.setTexture(PIXI.TextureCache['loginPressed']);
    },
    /**
     * Change button image in correlation to state
     */
    loginUp: function () {
        loginbutton.setTexture(PIXI.TextureCache['login']);
    }


};