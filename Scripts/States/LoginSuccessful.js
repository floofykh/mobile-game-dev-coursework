/**
 * Created by Christie on 16/03/2015.
 */

Game.LoginSuccessful = function(game){};
Game.LoginSuccessful.prototype = {

    /**
     * Display Images
     * Add button that will start the game
     * Handle selection of this button
     */
    create: function(){
        this.add.sprite(0, 0, 'background');

        var login = this.add.text(150, 100, 'LOGIN SUCCESSFUL', { fontSize: '48px',  fill: '#ffffff' });
        var name = this.add.text(0, 200, Game.playerName, { fontSize: '48px',  fill: '#ffffff' });
        name.position.x = Game.GAME_WIDTH/2 - name.width/2;

        mainmenubutton = this.add.button(Game.GAME_WIDTH - 525, Game.GAME_HEIGHT - 300, 'mainMenu', this.mainMenu, this);
        mainmenubutton.onInputOver.add(this.mainMenuOver, this);
        mainmenubutton.onInputOut.add(this.mainMenuOut, this);
        mainmenubutton.onInputDown.add(this.mainMenuDown, this);
        mainmenubutton.onInputUp.add(this.mainMenuUp, this);
    },

    /**
     * start the Game state
     */
    mainMenu: function() {
        this.state.start('MainMenu');
    },

    mainMenuOver: function () {
        mainmenubutton.setTexture(PIXI.TextureCache['mainMenuHover']);
    },
    /**
     * Change button image in correlation to state
     */
    mainMenuOut: function () {
        mainmenubutton.setTexture(PIXI.TextureCache['mainMenu']);
    },
    /**
     * Change button image in correlation to state
     */
    mainMenuDown: function () {
        mainmenubutton.setTexture(PIXI.TextureCache['mainMenuPressed']);
    },
    /**
     * Change button image in correlation to state
     */
    mainMenuUp: function () {
        mainmenubutton.setTexture(PIXI.TextureCache['mainMenu']);
    }
};