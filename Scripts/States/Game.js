Game.Game = function(game){
    // define needed variables for Game.Game
    this._player = null;
    this.otherPlayers =[];
    this._fontStyle = null;
    this.level = null;
    this.timer = null;
    this.mainmenubutton = null;
    this.pausebutton = null;
    this.movementPointerData = {id: null, startX: 0, startY: 0};
    this.shootPointerData = {id: null, startX: 0, startY: 0};
    Game._healthText = null;
    Game._scoreText = null;
    Game._pausedText = null;
    Game._score = 0;
    Game._health = 100;
    Game._lastPosition = 'up';
    Game._bulletTime = 0;
    Game._firingTimer = 0;
    Game._livingEnemies = [];
    var health;
    var score;
    var platforms;
    var bullets;
    var enemies;
    var goldBall;
    var enemyBullet;
    var map, floor, walls;
};
Game.Game.prototype = {
    create: function () {
        this.game.input.maxPointers = 2;
        this.game.input.addPointer();
        var _this = this;
        /**
         *  start the physics engine
         */
        this.physics.startSystem(Phaser.Physics.ARCADE);

        this.collectMusic = this.add.audio('collect');
        this.gamemusic = this.add.audio('game');
        if(Game.musicPlayable == true)
        {
            this.gamemusic.play("", 0, 1, true, true); //To loop sound
        }


        /**
         * Create and initialise the level
         * @type {Level}
         */
        this.level = new Level(this);
        this.level.intialise(['wall1'], ['floor1'], []);

        /**
         * Create Player and its settings
         */
        this._player = this.add.sprite(this.level.startPosition.x, this.level.startPosition.y, 'player');
        this.camera.follow(this._player);
        /**
         * We need to enable physics on the player
         */
        this.physics.arcade.enable(this._player);

        /**
         * Our two animations, walking left and right.
         */
        this._player.animations.add('right', [0, 1, 2, 3], 20, true, true);
        this._player.animations.add('left', [4, 5, 6, 7], 20, true, true);
        this._player.animations.add('up', [8, 9, 10, 11], 20, true, true);
        this._player.animations.add('down', [12, 13, 14, 15], 20, true, true);


        /**
         *   Our bullet group
         */
        bullets = this.add.group();
        bullets.enableBody = true;
        bullets.physicsBodyType = Phaser.Physics.ARCADE;
        bullets.createMultiple(10, 'bullet');
        bullets.setAll('anchor.x', 0.5);
        bullets.setAll('anchor.y', 1);
        bullets.setAll('outOfBoundsKill', true);
        bullets.setAll('checkWorldBounds', true);

        /**
         * The enemy's bullets
         */
        enemyBullets = this.add.group();
        enemyBullets.enableBody = true;
        enemyBullets.physicsBodyType = Phaser.Physics.ARCADE;
        enemyBullets.createMultiple(30, 'enemyBullet');
        enemyBullets.setAll('anchor.x', 0.5);
        enemyBullets.setAll('anchor.y', 1);
        enemyBullets.setAll('outOfBoundsKill', true);
        enemyBullets.setAll('checkWorldBounds', true);

        score = Game._score;
        health = 100;

        cursors = this.input.keyboard.createCursorKeys();
        fireButton = this.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        upLeftDiagonal = this.input.keyboard.addKey(Phaser.Keyboard.W);
        downLeftDiagonal = this.input.keyboard.addKey(Phaser.Keyboard.S);
        upRightDiagonal = this.input.keyboard.addKey(Phaser.Keyboard.E);
        downRightDiagonal = this.input.keyboard.addKey(Phaser.Keyboard.D);

        /**
         * Scoring System
         */
        this._scoreText = this.add.text(16, 16, 'Score: ' + Game._score, {fontSize: '32px', fill: '#ffffff'});
        this._scoreText.fixedToCamera = true;
        this._healthText = this.add.text(300, 16, 'Health: ' + Game._health, {fontSize: '32px', fill: '#ffffff'});
        this._healthText.fixedToCamera = true;

        this._pausedText = this.add.text(200, 250, "Game paused.\nTap anywhere to continue.", {
            fontSize: '32px',
            fill: '#ffffff'
        });
        this._pausedText.fixedToCamera = true;
        this._pausedText.visible = false;

        this.pausebutton = new Button(this, 753, 10, this.managePause, 'pause', 'pause', 'pauseHover', 'pausePressed');
        this.pausebutton.button.scale.x = 0.5;
        this.pausebutton.button.scale.y = 0.5;
        this.mainmenubutton =  new Button(this, 635, 10, this.mainMenuReturn, 'mainMenu', 'mainMenu', 'mainMenuHover', 'mainMenuPressed');
        this.mainmenubutton.button.scale.x = 0.5;
        this.mainmenubutton.button.scale.y = 0.5;
        this.mainmenubutton.fixedToCamera = true;



        /*this.timer = this.game.time.create(false);
         this.timer.loop(500, function(){
         if(Game.loggedIn){
         Game.socket.emit('playerPosition', {x: _this._player.x, y: _this._player.y});
         Game.socket.emit('getOtherPlayers');
         }
         }, this);

         if(Game.loggedIn){
         Game.socket.on('otherPlayers', function(data){
         if(Game.loggedIn){
         for(var i=0; i<data.length; i++){
         if(_this.otherPlayers.length <= i){
         _this.otherPlayers[i] = _this.add.sprite(_this.world.centerX, _this.world.centerY, 'player');
         }else{
         _this.otherPlayers[i].x = data.x;
         _this.otherPlayers[i].y = data.y;
         }
         }
         }
         });
         }
        this.timer.start();*/
    },

    /**
     * Pauses the game and displays pause text. Click anywhere to resume
     *  set event listener for the user's click/tap the screen
      */
    managePause: function () {
        this.game.paused = true;
        this.mainmenubutton.button.visible = false;
        this.pausebutton.button.visible = false;
        this._pausedText.visible = true;

        this.input.onDown.add( function () {
            this._pausedText.visible = false;
            this.mainmenubutton.button.visible = true;
            this.pausebutton.button.visible = true;
            this.game.paused = false;
        }, this);
    },

    mainMenuReturn: function() {
        Game._score = 0;
        this.gamemusic.stop();
        this.state.start('MainMenu');
    },

    movePlayer: function (pointer) {
        this._player.body.velocity.x = (pointer.position.x - pointer.positionDown.x);
        this._player.body.velocity.y = (pointer.position.y - pointer.positionDown.y);

        if (this._player.body.velocity.x > 0 && this._player.body.velocity.x > this._player.body.velocity.y) {
            this._player.animations.play('right');
            this._lastPosition = 'right';
        }

        else if (this._player.body.velocity.x < 0 && this._player.body.velocity.x < this._player.body.velocity.y) {
            this._player.animations.play('left');
            this._lastPosition = 'left';
        }

        else if (this._player.body.velocity.y < 0 && this._player.body.velocity.y < this._player.body.velocity.x) {
            this._player.animations.play('up');
            this._lastPosition = 'up';
        }

        else if (this._player.body.velocity.y > 0 && this._player.body.velocity.y > this._player.body.velocity.x) {
            this._player.animations.play('down');
            this._lastPosition = 'down';
        }
    },
    mainMenu: function () {
        Game._score = 0;
        this.state.start('MainMenu');
    },

    update: function () {

        this.physics.arcade.collide(this._player, this.level.wallsLayer);
        //  Reset the players velocity (movement)
        this._player.body.velocity.x = 0;
        this._player.body.velocity.y = 0;

        //this.game.debug.pointer(this.game.input.pointer1); //TEST
        //this.game.debug.pointer(this.game.input.pointer2);

        if (Phaser.Device.touch == true) { //MOBILE INPUT
            var pointer1 = this.input.pointer1;
            var pointer2 = this.input.pointer2;

            //Check if there is at least one touch on the screen
            if((pointer1.isDown && this.movementPointerData.id == null) || (pointer2.isDown && this.shootPointerData.id == null)){
                //If one is on the left side of the screen, do the movement
                if(pointer1.x < this.game.width/2){
                    this.movementPointerData.id = pointer1.id;
                    this.movementPointerData.startX = pointer1.x;
                    this.movementPointerData.startY = pointer1.y;
                    //this.movePlayer(pointer1);
                }
                else if(pointer2.x < this.game.width/2){
                    this.movementPointerData.id = pointer2.id;
                    this.movementPointerData.startX = pointer2.x;
                    this.movementPointerData.startY = pointer2.y;
                    //this.movePlayer(pointer2);
                }
                else {
                    this._player.body.velocity.x = 0;
                    this._player.body.velocity.y = 0;
                }

                //If one is on the right hand side, shoot
                if(pointer1.x > this.game.width/2){
                    this.shootPointerData.id = pointer1.id;
                    this.shootPointerData.startX = pointer1.x;
                    this.shootPointerData.startY = pointer1.y;
                    //this.fireBullet();
                }
                else if(pointer2.x > this.game.width/2){
                    this.shootPointerData.id = pointer2.id;
                    this.shootPointerData.startX = pointer2.x;
                    this.shootPointerData.startY = pointer2.y;
                    //this.fireBullet();
                }
            }

            if(this.input.pointer1.isUp){
                if(this.movementPointerData.id == this.input.pointer1.id){
                    this.movementPointerData.id = null;
                }
                if(this.shootPointerData.id == this.input.pointer1.id){
                    this.shootPointerData.id = null;
                }
            }
            if(this.input.pointer2.isUp){
                if(this.movementPointerData.id == this.input.pointer2.id){
                    this.movementPointerData.id = null;
                }
                if(this.shootPointerData.id == this.input.pointer2.id){
                    this.shootPointerData.id = null;
                }
            }

            //If there is a pointer which is controlling movement, move the player
            if(this.movementPointerData.id != null){
                var pointer = (this.input.pointer1.id == this.movementPointerData.id) ? this.input.pointer1 : this.input.pointer2;
                this.movePlayer(pointer);
            }

            if(this.shootPointerData.id != null){
                this.fireBullet();
            }

           /* if (this.input.pointer1.isDown) {

            }
            else {
                this._player.body.velocity.x = 0;
                this._player.body.velocity.y = 0;
            }*/


            //  Firing?
            /*if (this.input.pointer2.isDown) {
                this.fireBullet();
            }*/
        }
        else { //PC INPUT
            var cursorPressed = false;

            if (cursors.left.isDown) //Move left
            {
                this._player.body.velocity.x = -150;
                this._player.animations.play('left');
                this._lastPosition = 'left';
                cursorPressed = true;
            }

            if (cursors.right.isDown) {
                this._player.body.velocity.x = 150;
                this._player.animations.play('right');
                this._lastPosition = 'right';
                cursorPressed = true;
            }

            if (cursors.up.isDown) //Move Up
            {
                this._player.body.velocity.y = -150;
                this._player.animations.play('up');
                this._lastPosition = 'up';
                cursorPressed = true;
            }

            if (cursors.down.isDown) //Move Down
            {
                this._player.body.velocity.y = 150;
                this._player.animations.play('down');
                this._lastPosition = 'down';
                cursorPressed = true;
            }

            /*else if (upLeftDiagonal.isDown) {
                this._player.body.velocity.y = -150;
                this._player.body.velocity.x = -150
                this._player.animations.play('up');
                this._lastPosition = 'up';
            }

            else if (downLeftDiagonal.isDown) {
                this._player.body.velocity.y = 150;
                this._player.body.velocity.x = -150
                this._player.animations.play('down');
                this._lastPosition = 'down';
            }

            else if (upRightDiagonal.isDown) {
                this._player.body.velocity.y = -150;
                this._player.body.velocity.x = 150
                this._player.animations.play('up');
                this._lastPosition = 'up';
            }

            else if (downRightDiagonal.isDown) {
                this._player.body.velocity.y = 150;
                this._player.body.velocity.x = 150
                this._player.animations.play('down');
                this._lastPosition = 'down';
            }*/

            if(cursorPressed == false) //Stand Still
            {
                this._player.animations.stop();
            }

            if (fireButton.isDown) {
                this.fireBullet();
            }
        }


        if (this.time.now > Game._firingTimer) {
            this.enemyFires();
        }

        //Check physics between enemies and player
        this.physics.arcade.overlap(this._player, this.level.enemies, depleteHealth, null, this);

        //Check physics between enemies and bullets
        this.physics.arcade.overlap(bullets, this.level.enemies, killEnemies, null, this);

        //Check if enemy bullets hit the player
        this.physics.arcade.overlap(this._player, enemyBullets, hitPlayer, null, this);

        //Check if player is overlapping stars and if so, call collectStar function
        this.physics.arcade.overlap(this._player, this.level.collectibles, collectStar, null, this);

        //Check if player is overlapping gold ball and if so, call win function
        this.physics.arcade.overlap(this._player, this.level.exit, collectBall, null, this);

        //Check if enemy bullets hit the walls
        this.physics.arcade.overlap(this.level.wallsLayer, enemyBullets, killBullet, null, this);

        //Check if player bullets hit the walls
        this.physics.arcade.overlap(this.level.wallsLayer, bullets, killBullet, null, this);

    },

    /**
     * Fire bullet if possible to fire
     * To avoid them being allowed to fire too fast we set a time limit
     * Grab the first bullet we can from the pool
     */
    fireBullet: function () {

        if (this.time.now > Game._bulletTime) {
            var bullet = bullets.getFirstExists(false);

            if (bullet) {
                bullet.reset(this._player.body.center.x, this._player.body.center.y);


                if (this._lastPosition == 'down') {
                    bullet.body.velocity.y = 800;
                }
                else if (this._lastPosition == 'left') {
                    bullet.body.velocity.x = -800;
                }
                else if (this._lastPosition == 'right') {
                    bullet.body.velocity.x = 800;
                }
                else {
                    bullet.body.velocity.y = -800
                }
                Game._bulletTime = this.time.now + 200;
            }
        }
    },

    /**
     * Random enemy fire bullet if possible
     * Grab the first bullet we can from the pool
     * Put every living enemy in an array
     * Randomly select one of them
     * And fire the bullet from this enemy
     */
    enemyFires: function () {
        enemyBullet = enemyBullets.getFirstExists(false);

        Game._livingEnemies.length = 0;

        this.level.enemies.forEachAlive(function(enemy){
            Game._livingEnemies.push(enemy);
        });


        if (enemyBullet && Game._livingEnemies.length > 0)
        {
            var random = this.rnd.integerInRange(0, Game._livingEnemies.length-1);
            var shooter = Game._livingEnemies[random];

            enemyBullet.reset(shooter.body.x, shooter.body.y);

            this.physics.arcade.moveToObject(enemyBullet, this._player, 120);
            Game._firingTimer = this.time.now + 2000;
        }

    },

    updateServer: function(){
        if(Game.loggedIn){
            Game.socket.emit('playerPosition', {x: this._player.x, y: this._player.y});
            Game.socket.emit('getOtherPlayers');
        }
    }

};

/**
 * Testing function that adds bounding boxes to the player and the bullets
 * @param bullet
 * @param enemy
 */
/*render: function(){
     this.game.debug.body(this._player);
     this.game.debug.body(enemyBullet);
     }   */

/**
 * If bullet collides with enemy then destroy both
 * Increment score and update on screen
 * @param bullet
 * @param enemy
 */
function killEnemies (bullet, enemy) {

    enemy.kill();
    bullet.kill();

    score += 50;
    Game._score = score;
    this._scoreText.text = 'Score: ' + score;
}

function killBullet (bullet, platforms) {

    bullet.kill();
}


function depleteHealth (player, enemy) {

    //Change to bullet later
    enemy.kill();

    //Deplete health and update text on screen
    health -= 40;
    this._healthText.text = 'Health: ' + health;

    if(health <= 0) {
        this.gamemusic.stop();
        this.state.start('GameOver');
    }
}

function hitPlayer (player, enemybullets) {

    //Change to bullet later
    enemybullets.kill();

    //Deplete health and update text on screen
    health -= 10;
    this._healthText.text = 'Health: ' + health;

    if(health <= 0) {
        this.gamemusic.stop();
        this.state.start('GameOver');
    }
}


function collectStar (player, star) {

    star.kill();

    //  Add and update the score
    if(Game.soundPlayable == true)
    {
        this.collectMusic.play();
    }

    score += 10;
    Game._score = score;
    //this._scoreText.text = 'Score: ' + this._score;
    this._scoreText.text = 'Score: ' + score;

    //if(score >= 120) {
    //  this.gamemusic.stop();
    // this.state.start('LevelComplete');
    //}
}


function collectBall (player, ball) {
    ball.kill();

    //  Add and update the score
    if(Game.soundPlayable == true)
    {
        this.collectMusic.play();
    }
    score += 1000;
    Game._score = score;
    this._scoreText.text = 'Score: ' + score;
    this.gamemusic.stop();
    this.state.start('LevelComplete');
}


function resetBullet (bullet) {

    //  Called if the bullet goes out of the screen
    bullet.kill();

}