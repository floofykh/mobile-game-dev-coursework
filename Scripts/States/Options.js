/**
 * Created by Christie on 16/03/2015.
 */

Game.Options = function(game){};
Game.Options.prototype = {
    /**
     * Create music, nomusic, sounds, nosounds and mainmenu
     * Display correct buttons according to sound and music states
     */
    create: function(){
        this.add.sprite(0, 0, 'background');

        var options = this.add.text(280, 50, 'OPTIONS', { fontSize: '48px',  fill: '#ffffff' });

        musicbutton = this.add.button(Game.GAME_WIDTH - 300, Game.GAME_HEIGHT - 400, 'musicON', this.music, this);
        musicbutton.onInputOver.add(this.musicOver, this);
        musicbutton.onInputOut.add(this.musicOut, this);
        musicbutton.onInputDown.add(this.musicDown, this);
        musicbutton.onInputUp.add(this.musicUp, this);

        nomusicbutton = this.add.button(Game.GAME_WIDTH - 300, Game.GAME_HEIGHT - 400, 'musicOFF', this.music, this);
        nomusicbutton.onInputOver.add(this.nomusicOver, this);
        nomusicbutton.onInputOut.add(this.nomusicOut, this);
        nomusicbutton.onInputDown.add(this.nomusicDown, this);
        nomusicbutton.onInputUp.add(this.nomusicUp, this);

        if(Game.musicPlayable == true)
        {
            nomusicbutton.visible = false;
        }
        else
        {
            musicbutton.visible = false;
        }

        soundbutton = this.add.button(Game.GAME_WIDTH - 700, Game.GAME_HEIGHT - 400, 'soundsON', this.sounds, this);
        soundbutton.onInputOver.add(this.soundOver, this);
        soundbutton.onInputOut.add(this.soundOut, this);
        soundbutton.onInputDown.add(this.soundDown, this);
        soundbutton.onInputUp.add(this.soundUp, this);

        nosoundbutton = this.add.button(Game.GAME_WIDTH - 700, Game.GAME_HEIGHT - 400, 'soundsOFF', this.sounds, this);
        nosoundbutton.onInputOver.add(this.nosoundOver, this);
        nosoundbutton.onInputOut.add(this.nosoundOut, this);
        nosoundbutton.onInputDown.add(this.nosoundDown, this);
        nosoundbutton.onInputUp.add(this.nosoundUp, this);

        if(Game.soundPlayable == true)
        {
            nosoundbutton.visible = false;
        }
        else
        {
            soundbutton.visible = false;
        }

        mainmenubutton = this.add.button(Game.GAME_WIDTH - 525, Game.GAME_HEIGHT - 200, 'mainMenu', this.mainMenu, this);
        mainmenubutton.onInputOver.add(this.mainMenuOver, this);
        mainmenubutton.onInputOut.add(this.mainMenuOut, this);
        mainmenubutton.onInputDown.add(this.mainMenuDown, this);
        mainmenubutton.onInputUp.add(this.mainMenuUp, this);
    },

    /**
     * Start the Main Menu state
     */
    mainMenu: function() {
        this.state.start('MainMenu');
    },

    /**
     * Music ON/OFF and display correct buttons
     */
    music: function() {
        if(Game.musicPlayable == true)
        {
            Game.musicPlayable = false;
            musicbutton.visible = false;
            nomusicbutton.visible = true;
        }
        else
        {
            Game.musicPlayable = true;
            musicbutton.visible = true;
            nomusicbutton.visible = false;
        }
    },

    /**
     * Sounds ON/OFF and display correct buttons
     */
    sounds: function() {
        if(Game.soundPlayable == true)
        {
            Game.soundPlayable = false;
            soundbutton.visible = false;
            nosoundbutton.visible = true
        }
        else
        {
            Game.soundPlayable = true;
            soundbutton.visible = true;
            nosoundbutton.visible = false;
        }
    },

    /**
     * Change button image in correlation to state
     */
    musicOver: function () {
        musicbutton.setTexture(PIXI.TextureCache['musicONHover']);
    },
    /**
     * Change button image in correlation to state
     */
    musicOut: function () {
        musicbutton.setTexture(PIXI.TextureCache['musicON']);
    },
    /**
     * Change button image in correlation to state
     */
    musicDown: function () {
        musicbutton.setTexture(PIXI.TextureCache['musicONPressed']);
    },
    /**
     * Change button image in correlation to state
     */
    musicUp: function () {
        musicbutton.setTexture(PIXI.TextureCache['musicON']);
    },
    /**
     * Change button image in correlation to state
     */
    nomusicOver: function () {
        nomusicbutton.setTexture(PIXI.TextureCache['musicOFFHover']);
    },
    /**
     * Change button image in correlation to state
     */
    nomusicOut: function () {
        nomusicbutton.setTexture(PIXI.TextureCache['musicOFF']);
    },
    /**
     * Change button image in correlation to state
     */
    nomusicDown: function () {
        nomusicbutton.setTexture(PIXI.TextureCache['musicOFFPressed']);
    },
    /**
     * Change button image in correlation to state
     */
    nomusicUp: function () {
        nomusicbutton.setTexture(PIXI.TextureCache['musicOFF']);
    },
    /**
     * Change button image in correlation to state
     */
    soundOver: function () {
        soundbutton.setTexture(PIXI.TextureCache['soundsONHover']);
    },
    /**
     * Change button image in correlation to state
     */
    soundOut: function () {
        soundbutton.setTexture(PIXI.TextureCache['soundsON']);
    },
    /**
     * Change button image in correlation to state
     */
    soundDown: function () {
        soundbutton.setTexture(PIXI.TextureCache['soundsONPressed']);
    },
    /**
     * Change button image in correlation to state
     */
    soundUp: function () {
        soundbutton.setTexture(PIXI.TextureCache['soundsON']);
    },
    /**
     * Change button image in correlation to state
     */
    nosoundOver: function () {
        nosoundbutton.setTexture(PIXI.TextureCache['soundsOFFHover']);
    },
    /**
     * Change button image in correlation to state
     */
    nosoundOut: function () {
        nosoundbutton.setTexture(PIXI.TextureCache['soundsOFF']);
    },
    /**
     * Change button image in correlation to state
     */
    nosoundDown: function () {
        nosoundbutton.setTexture(PIXI.TextureCache['soundsOFFPressed']);
    },
    /**
     * Change button image in correlation to state
     */
    nosoundUp: function () {
        nosoundbutton.setTexture(PIXI.TextureCache['soundsOFF']);
    },
    /**
     * Change button image in correlation to state
     */
    mainMenuOver: function () {
        mainmenubutton.setTexture(PIXI.TextureCache['mainMenuHover']);
    },
    /**
     * Change button image in correlation to state
     */
    mainMenuOut: function () {
        mainmenubutton.setTexture(PIXI.TextureCache['mainMenu']);
    },
    /**
     * Change button image in correlation to state
     */
    mainMenuDown: function () {
        mainmenubutton.setTexture(PIXI.TextureCache['mainMenuPressed']);
    },
    /**
     * Change button image in correlation to state
     */
    mainMenuUp: function () {
        mainmenubutton.setTexture(PIXI.TextureCache['mainMenu']);
    }
};