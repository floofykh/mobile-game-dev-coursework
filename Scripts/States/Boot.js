/**
 * Created by Christie on 14/03/2015.
 */

var Game = {};
Game.loggedIn = false;
Game.socket = null;
Game.playerName = null;
Game.Boot = function(game){};
Game.Boot.prototype = {
    preload: function(){
        // preload the loading indicator first before anything else
        this.load.image('preloaderBar', 'Images/loading-bar.png');
    },
    create: function(){
        // set scale options
        this.scale.scaleMode = Phaser.ScaleManager.EXACT_FIT;
        this.scale.setScreenSize(true);
        //this.scale.setMinMax(480, 320, 1152, 768);
        this.scale.pageAlignHorizontally = true;
        this.scale.pageAlignVertically = true;
        this.scale.forceOrientation(true, false);
        this.scale.forceLandscape = true; 
        this.scale.refresh();
        //this.scale.setScreenSize(true);

        //Connect
        Game.socket = io();
        var _this = this;
        Game.socket.on('connected', function(){
            Game.loggedIn = true;
            _this.state.start('Preloader');
        });
        Game.socket.on('connectFailed', function(){
            Game.loggedIn = false;
            _this.state.start('Preloader');
        });

    }
};