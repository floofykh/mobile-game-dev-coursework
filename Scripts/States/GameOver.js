/**
 * Created by Christie on 16/03/2015.
 */

Game.GameOver = function(game){};
Game.GameOver.prototype = {

    /**
     *  Displays images and buttons
     *  Displays user's score
     */
    create: function(){
        this.add.sprite(0, 0, 'background');
        var gameover = this.add.text(250, 75, 'GAME OVER', { fontSize: '48px',  fill: '#ffffff' });
        this._scoreText = this.add.text(275, 225, 'Score: ' + Game._score, { fontSize: '50px', fill: '#ffffff'} );

        if(Game.loggedIn){
            Game.socket.emit('sendNewScore', {score: Game._score, name: Game.playerName});
        }

        mainmenubutton = this.add.button(Game.GAME_WIDTH - 350, Game.GAME_HEIGHT - 150, 'mainMenu', this.mainMenu, this);
        mainmenubutton.onInputOver.add(this.mainMenuOver, this);
        mainmenubutton.onInputOut.add(this.mainMenuOut, this);
        mainmenubutton.onInputDown.add(this.mainMenuDown, this);
        mainmenubutton.onInputUp.add(this.mainMenuUp, this);

        restartbutton = this.add.button(Game.GAME_WIDTH - 700, Game.GAME_HEIGHT - 150, 'restart', this.restart, this);
        restartbutton.onInputOver.add(this.restartOver, this);
        restartbutton.onInputOut.add(this.restartOut, this);
        restartbutton.onInputDown.add(this.restartDown, this);
        restartbutton.onInputUp.add(this.restartUp, this);
    },
    /**
     * Resets players score and restarts level
     */
    restart: function() {
        Game._score = 0;
        this.state.start('Game');
    },
    /**
     *  Go to Main Menu Screen + Reset score
     */
    mainMenu: function() {
        Game._score = 0;
        this.state.start('MainMenu');
    },


    /**
     * Change button image in correlation to state
     */
    mainMenuOver: function () {
        mainmenubutton.setTexture(PIXI.TextureCache['mainMenuHover']);
    },
    /**
     * Change button image in correlation to state
     */
    mainMenuOut: function () {
        mainmenubutton.setTexture(PIXI.TextureCache['mainMenu']);
    },
    /**
     * Change button image in correlation to state
     */
    mainMenuDown: function () {
        mainmenubutton.setTexture(PIXI.TextureCache['mainMenuPressed']);
    },
    /**
     * Change button image in correlation to state
     */
    mainMenuUp: function () {
        mainmenubutton.setTexture(PIXI.TextureCache['mainMenu']);
    },
    /**
     * Change button image in correlation to state
     */
    restartOver: function () {
        restartbutton.setTexture(PIXI.TextureCache['restartHover']);
    },
    /**
     * Change button image in correlation to state
     */
    restartOut: function () {
        restartbutton.setTexture(PIXI.TextureCache['restart']);
    },
    /**
     * Change button image in correlation to state
     */
    restartDown: function () {
        restartbutton.setTexture(PIXI.TextureCache['restartPressed']);
    },
    /**
     * Change button image in correlation to state
     */
    restartUp: function () {
        restartbutton.setTexture(PIXI.TextureCache['restart']);
    }
};