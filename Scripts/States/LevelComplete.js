/**
 * Created by Christie on 16/03/2015.
 */

Game.LevelComplete = function(game){};
Game.LevelComplete.prototype = {

    /**
     * Display images + buttons
     * Check if Music is playable and if so, play it
     * Display score for player
     */
    create: function(){
        this.add.sprite(0, 0, 'background');
        this.completemusic = this.add.audio('levelcomplete');
        if(Game.musicPlayable == true)
        {
            this.completemusic.play("", 0, 1, true, true);
        }

        var complete = this.add.text(190, 100, 'LEVEL COMPLETE', { fontSize: '48px',  fill: '#ffffff' });
        this._scoreText = this.add.text(250, 250, 'Score: ' + Game._score, { fontSize: '50px', fill: '#ffffff'} );


        mainmenubutton = this.add.button(Game.GAME_WIDTH - 350, Game.GAME_HEIGHT - 150, 'mainMenu', this.mainMenu, this);
        mainmenubutton.onInputOver.add(this.mainMenuOver, this);
        mainmenubutton.onInputOut.add(this.mainMenuOut, this);
        mainmenubutton.onInputDown.add(this.mainMenuDown, this);
        mainmenubutton.onInputUp.add(this.mainMenuUp, this);

        nextlevelbutton = this.add.button(Game.GAME_WIDTH - 700, Game.GAME_HEIGHT - 150, 'nextLevel', this.nextLevel, this);
        nextlevelbutton.onInputOver.add(this.nextLevelOver, this);
        nextlevelbutton.onInputOut.add(this.nextLevelOut, this);
        nextlevelbutton.onInputDown.add(this.nextLevelDown, this);
        nextlevelbutton.onInputUp.add(this.nextLevelUp, this);
    },

    /**
     * Stops music and takes user to next level
     */
    nextLevel: function() {
        this.completemusic.stop();
        this.state.start('Game');
    },

    /**
     * Stpps music, resets score and takes user to main menu
     */
    mainMenu: function() {
        this.completemusic.stop();
        Game._score = 0;
        this.state.start('MainMenu');
    },

    /**
     * Change button image in correlation to state
     */
    mainMenuOver: function () {
        mainmenubutton.setTexture(PIXI.TextureCache['mainMenuHover']);
    },
    /**
     * Change button image in correlation to state
     */
    mainMenuOut: function () {
        mainmenubutton.setTexture(PIXI.TextureCache['mainMenu']);
    },
    /**
     * Change button image in correlation to state
     */
    mainMenuDown: function () {
        mainmenubutton.setTexture(PIXI.TextureCache['mainMenuPressed']);
    },
    /**
     * Change button image in correlation to state
     */
    mainMenuUp: function () {
        mainmenubutton.setTexture(PIXI.TextureCache['mainMenu']);
    },
    /**
     * Change button image in correlation to state
     */
    nextLevelOver: function () {
        nextlevelbutton.setTexture(PIXI.TextureCache['nextLevelHover']);
    },
    /**
     * Change button image in correlation to state
     */
    nextLevelOut: function () {
        nextlevelbutton.setTexture(PIXI.TextureCache['nextLevel']);
    },
    /**
     * Change button image in correlation to state
     */
    nextLevelDown: function () {
        nextlevelbutton.setTexture(PIXI.TextureCache['nextLevelPressed']);
    },
    /**
     * Change button image in correlation to state
     */
    nextLevelUp: function () {
        nextlevelbutton.setTexture(PIXI.TextureCache['nextLevel']);
    }
};