/**
 * Created by Christie on 13/03/2015.
 */

Game.MainMenu = function(game){};
Game.MainMenu.prototype = {

    create: function(){
        // display images
        var background = this.add.tileSprite(0, 0, Game.GAME_WIDTH, Game.GAME_HEIGHT,  'background');
        
        var dungeon = this.add.text(150, 80, 'DUNGEON CRAWLER', { fontSize: '48px',  fill: '#ffffff' });
        this.menumusic = this.add.audio('mainmenu');
        if(Game.musicPlayable == true)
        {
            this.menumusic.play("", 0, 1, true, true);
        }

        //Create Buttons
        new Button(this, Game.GAME_WIDTH-525, Game.GAME_HEIGHT-400, this.startGame, 'play', 'play', 'playHover', 'playPressed');
        new Button(this, Game.GAME_WIDTH-525, Game.GAME_HEIGHT-300, this.startLoginScreen, 'login', 'login', 'loginHover', 'loginPressed');
        new Button(this, Game.GAME_WIDTH-525, Game.GAME_HEIGHT-200, this.startOptions, 'options', 'options', 'optionsHover', 'optionsPressed');
        new Button(this, Game.GAME_WIDTH-525, Game.GAME_HEIGHT-100, this.startHighScores, 'highscores', 'highscores', 'highscoresHover', 'highscoresPressed');
    },
    startGame: function() {
        // start the Game state and stop music
        this.menumusic.stop();
        this.state.start('Game');
    },
    startOptions: function() {
        //Go to Options Screen
        this.menumusic.stop();
        this.state.start('Options');
    },

    startHighScores: function() {
        //Go to high-score screen
        this.menumusic.stop();
        this.state.start('HighScore');
    },
    startLoginScreen: function() {
        this.menumusic.stop();
        this.state.start('Login');
    }
};