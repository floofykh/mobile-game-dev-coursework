/**
 * Created by Christie on 13/03/2015.
 */

module.exports = {
    preload: function() {
        game.load.image('login', 'Images/Buttons/Login.png');
        game.load.image('loginHover', 'Images/Buttons/LoginHover.png');
        game.load.image('loginPressed', 'Images/Buttons/LoginPressed.png');

        game.load.image('mainMenu', 'Images/Buttons/MainMenu.png');
        game.load.image('mainMenuHover', 'Images/Buttons/MainMenuHover.png');
        game.load.image('mainMenuPressed', 'Images/Buttons/MainMenuPressed.png');

        game.load.image('music', 'Images/Buttons/Music.png');
        game.load.image('musicHover', 'Images/Buttons/MusicHover.png');
        game.load.image('musicPressed', 'Images/Buttons/MusicPressed.png');

        game.load.image('next', 'Images/Buttons/Next.png');
        game.load.image('nextHover', 'Images/Buttons/NextHover.png');
        game.load.image('nextPressed', 'Images/Buttons/NextPressed.png');

        game.load.image('nextLevel', 'Images/Buttons/NextLevel.png');
        game.load.image('nextLevelHover', 'Images/Buttons/NextLevelHover.png');
        game.load.image('nextLevelPressed', 'Images/Buttons/NextLevelPressed.png');

        game.load.image('sounds', 'Images/Buttons/Sounds.png');
        game.load.image('soundsHover', 'Images/Buttons/soundsHover.png');
        game.load.image('soundsPressed', 'Images/Buttons/SoundsPressed.png');

        game.load.image('noSounds', 'Images/Buttons/NoSounds.png');
        game.load.image('noSoundsHover', 'Images/Buttons/noSoundsHover.png');
        game.load.image('noSoundsPressed', 'Images/Buttons/noSoundsPressed.png');

        game.load.image('pause', 'Images/Buttons/Pause.png');
        game.load.image('pauseHover', 'Images/Buttons/PauseHover.png');
        game.load.image('pausePressed', 'Images/Buttons/PausePressed.png');

        game.load.image('play', 'Images/Buttons/Play.png');
        game.load.image('playHover', 'Images/Buttons/PlayHover.png');
        game.load.image('playPressed', 'Images/Buttons/PlayPressed.png');

        game.load.image('previous', 'Images/Buttons/Previous.png');
        game.load.image('previousHover', 'Images/Buttons/PreviousHover.png');
        game.load.image('previousPressed', 'Images/Buttons/PreviousPressed.png');

        game.load.image('restart', 'Images/Buttons/RestartLevel.png');
        game.load.image('restartHover', 'Images/Buttons/RestartHover.png');
        game.load.image('restartPressed', 'Images/Buttons/RestartPressed.png');

        game.load.image('sky', 'Images/sky.png');
        game.load.image('ground', 'Images/platform.png');
        game.load.image('star', 'Images/star.png');
        game.load.spritesheet('dude', 'Images/dude.png', 32, 48);
    }
};

//Even if we don't load them all at once, this code can be copy + pasted instead of having to type it up every time :)