/**
 * Created by R2D2 on 13/04/2015.
 */
function Button(game, x, y, onClick, name, normalTexture, hoverTexture, pressTexture){
    this.button = game.add.button(x, y, name, onClick, game);
    this.normalTexture = normalTexture;
    this.hoverTexture = hoverTexture;
    this.pressTexture = pressTexture;

    var self = this;

    self.button.fixedToCamera = true;

    this.button.onInputOver.add(function(){
        self.button.setTexture(PIXI.TextureCache[hoverTexture])
    }, game);

    this.button.onInputOut.add(function(){
        self.button.setTexture(PIXI.TextureCache[normalTexture])
    }, game);

    this.button.onInputDown.add(function(){
        self.button.setTexture(PIXI.TextureCache[pressTexture])
    }, game);

    this.button.onInputUp.add(function(){
        self.button.setTexture(PIXI.TextureCache[hoverTexture])
    }, game);
}