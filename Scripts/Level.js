/**
 * Created by R2D2 on 13/04/2015.
 */

/**
 * 
 * @param state - The current state object of the game
 * @constructor
 */
Level = function(state){
    this.state = state;
    this.tileWidth = 32;
    this.tileHeight = 32;
    this.map = null;
    this.floorLayer = null;
    this.wallsLayer = null;
    this.levelWidth = 0;
    this.levelHeight = 0;
    this.enemies = null;
    this.collectibles = null;
    this.exit;
    this.startPosition = null;
    this.tileContents = null;
    this.exitCreated = false;
}

Level.prototype = {
    /**
     * Initialises the contents of the level to the basic layout, only called internally.
     */
    initialiseLevelData: function () {
        this.tileContents = createArray(this.levelWidth, this.levelHeight);

        for (var x = 0; x < this.levelWidth; x++) {
            for (var y = 0; y < this.levelHeight; y++) {
                if (x === 0 || y === 0 || x === this.levelWidth-1 || y === this.levelHeight-1) {
                    this.tileContents[x][y] = 'wall';
                }
                else {
                    this.tileContents[x][y] = 'floor';
                }
            }
        }
    },
    /**
     * Used in generating the world to move the index to the next tile in a direction
     * @param direction - The direction to move in. 0 = left, 1 = up, 2 = right, 3 = down.
     * @param currentTile - An array holding the current tile.
     * @returns {int[]} - The new position
     */
    moveInDirection: function (direction, currentTile) {
        switch (direction) {
            case 0:
                currentTile.x--;
                break;
            case 1:
                currentTile.y++;
                break;
            case 2:
                currentTile.x++;
                break;
            case 3:
                currentTile.y--;
                break;
        }
        return currentTile;
    },
    /**
     * Returns a direction that is perpendicular to that given
     * @param direction
     * @returns {integer}
     */
    rightAngleDirection: function (direction) {
        var flipACoin = this.state.rnd.frac();
        if (flipACoin < 0.5) {
            direction = (direction + 1) % 3
        }
        else {
            direction = (direction - 1) % 3
        }
        return direction;
    },
    /**
     * Returns the data for a specified wall.
     * @param startingWall - 0 = left, 1 = top, 2 = right, 3 = bottom
     * @returns {{minX: integer, minY: integer, maxX: integer, maxY: integer, direction: integer}}
     */
    getDataForWall: function (startingWall) {
        var minX, minY, maxX, maxY, direction;
        switch (startingWall) {
            case 0:
                minX = 0;
                maxX = 0;
                minY = 0;
                maxY = this.levelHeight;
                direction = 2;
                break;
            case 1:
                minX = 0;
                maxX = this.levelWidth;
                minY = this.levelHeight;
                maxY = this.levelHeight;
                direction = 3;
                break;
            case 2:
                minX = this.levelWidth;
                maxX = this.levelWidth;
                minY = 0;
                maxY = this.levelHeight;
                direction = 0;
                break;
            case 3:
                minX = 0;
                maxX = this.levelWidth;
                minY = 0;
                maxY = 0;
                direction = 1;
                break;
        }
        return {minX: minX, minY: minY, maxX: maxX, maxY: maxY, direction: direction};
    },

    /**
     *
     * @param x {number} - Tile index on the x-axis
     * @param y {number} - Tile index on the y-axis
     * @param minX {number} - The minimum X coordinate for the room (Left)
     * @param maxX {number} - The maximum X coordinate for the room (Right)
     * @param minY {number} - The minimum Y coordinate for the room (Bottom)
     * @param maxY {number} - The maximum Y coordinate for the room (Top)
     * @returns {boolean} - True if it's a corner, false otherwise
     */
    isCorner: function(x, y, minX, maxX, minY, maxY){
        if(x === minX && y === minY){
            return true;
        }
        if(x === minX && y === maxY-1){
            return true;
        }
        if(x === maxX-1 && y === minY){
            return true;
        }
        if(x === maxX-1 && y === maxY-1){
            return true;
        }
        return false;
    },
    /**
     * Generates a room at the position.
     * @param x - x coordinate of the centre
     * @param y - y coordinate of the centre
     * @param minWidth
     * @param minHeight
     * @param maxWidth
     * @param maxHeight
     */
    generateRoom: function(x, y, width, height) {
        var minX = Math.max(2, x - Math.floor(width/2));
        var minY = Math.max(2, y - Math.floor(height/2));
        var maxX = Math.min(this.levelWidth-2, x + Math.ceil(width/2));
        var maxY = Math.min(this.levelWidth-2, y + Math.ceil(height/2));
        var entranceCreated = false;
        var entranceIndex = this.state.rnd.integerInRange(0, (maxX-minX-2)*2 + (maxY-minY-2)*2); //Subtract four to not count the corners.
        var wallIndex = 0;

        //I hate these if statements so much, but can't be bothered cleaning it up at the moment.
        for(var i = minX; i<maxX; i++){
            for(var j = minY; j<maxY; j++){
                if(this.tileContents[i][j] == 'floor'){
                    if((i === minX || i === maxX-1 || j === minY || j == maxY-1)){ //If this is a boundary i.e wall
                        if(!entranceCreated && !this.isCorner(i, j, minX, maxX, minY, maxY)){ //If the entrance has not been created and this is not a corner. (Ugly, I know...)
                            if(wallIndex === entranceIndex){//If this wall is selected as the entrance, or if it's the final wall
                                this.tileContents[i][j] = 'entrance';
                                //entranceCreated = true;
                            }
                            else{
                                this.tileContents[i][j] = 'wall';
                            }
                            wallIndex++;
                        }
                        else{
                            this.tileContents[i][j] = 'wall';
                        }
                    }
                    else{
                        this.tileContents[i][j] = 'room';
                    }
                }
            }
        }

        var roomWidth = maxX - minX;
        var roomHeight = maxY - minY;
        var centreX = minX + roomWidth/2;
        var centreY = minY + roomHeight/2;
        this.generateEnemies(centreX, centreY, roomWidth-4, roomHeight-4);
        this.generateCollectibles(centreX, centreY, roomWidth-4, roomHeight-4);
    },
    /**
     * Generates the entire level.
     */
    generateLevel: function () {
        var minRoomSize = 5, maxRoomSize = 10;
        var numHorRooms = Math.floor(this.levelWidth / (maxRoomSize));
        var numVerRooms = Math.floor(this.levelHeight / (maxRoomSize));

        var exitRoom = this.state.rnd.integerInRange(0, numHorRooms*numVerRooms-1);
        var startRoom = this.state.rnd.integerInRange(0, numHorRooms*numVerRooms-1);

        var roomCount = 0;
        for(var x= 0; x < numHorRooms; x++){
            for(var y=0; y<numVerRooms; y++){
                var positionX = (maxRoomSize+1) * (x+1);
                var positionY = (maxRoomSize+1) * (y+1) - maxRoomSize/2;

                var roomWidth = this.state.rnd.integerInRange(minRoomSize, maxRoomSize);
                var roomHeight = this.state.rnd.integerInRange(minRoomSize, maxRoomSize);

                this.generateRoom(positionX, positionY, roomWidth, roomHeight);
                if(roomCount == exitRoom){
                    this.exit.create(positionX*this.tileWidth, positionY*this.tileHeight, 'goal');
                }
                if(roomCount == startRoom){
                    this.startPosition = {x: positionX*this.tileWidth, y: positionY*this.tileHeight};
                }
                roomCount++;
            }
        }
    },
    /**
     * Populates the TileMap with either walls or floors.
     */
    populateTileMap: function () {
        for (var x = 0; x < this.levelWidth; x++) {
            for (var y = 0; y < this.levelHeight; y++) {
                this.map.putTile(0, x, y, this.floorLayer);
                if (this.tileContents[x][y] === 'wall') {
                    this.map.putTile(1, x, y, this.wallsLayer);
                }
            }
        }
    },
    /**
     * Generates a random amount of enemies in an area.
     * @param positionX {Number} - Centre of the area on the x-axis
     * @param positionY {Number} - Centre of the area on the y-axis
     * @param width {Number} - Width of the area
     * @param height {Number} - Height of the areas
     */
    generateEnemies: function (positionX, positionY, width, height) {
        //Create enemies
        var numEnemies = this.state.rnd.integerInRange(0, (width*height)/4);
        var minX = Math.max(1, Math.ceil(positionX - width/2));
        var minY = Math.max(1, Math.ceil(positionY - height/2));
        var maxX = Math.min(this.levelWidth-1, Math.floor(positionX + width/2));
        var maxY = Math.min(this.levelHeight-1, Math.floor(positionY + height/2));
        for (var i = 0; i < numEnemies; i++) {
            var x = this.state.rnd.integerInRange(minX, maxX);
            var y = this.state.rnd.integerInRange(minY, maxY);
            this.enemies.create(x*this.tileWidth, y*this.tileHeight, 'spider');
        }
    },
    /**
     * Generates a random amount of collectibles in an area.
     * @param positionX {Number} - Centre of the area on the x-axis
     * @param positionY {Number} - Centre of the area on the y-axis
     * @param width {Number} - Width of the area
     * @param height {Number} - Height of the areas
     */
    generateCollectibles: function (positionX, positionY, width, height) {
        var numCollectibles = this.state.rnd.integerInRange(0, (width*height)/4);
        var minX = Math.max(1, Math.ceil(positionX - width/2));
        var minY = Math.max(1, Math.ceil(positionY - height/2));
        var maxX = Math.min(this.levelWidth-1, Math.floor(positionX + width/2));
        var maxY = Math.min(this.levelHeight-1, Math.floor(positionY + height/2));
        for (var i = 0; i < numCollectibles; i++) {
            var x = this.state.rnd.integerInRange(minX, maxX);
            var y = this.state.rnd.integerInRange(minY, maxY);
            this.collectibles.create(x*this.tileWidth, y*this.tileHeight, 'star');
        }
    },
    /**
     * Initialises the level with the current parameters.
     * @param walls - An array of strings containing the names of different wall tiles
     * @param floors - An array of strings containing the names of different floor tiles
     * @param enemies - An array of string containing the names of different enemy sprites
     */
    intialise: function(walls, floors, enemies) {
        var wallIndex = this.state.rnd.integerInRange(0, walls.length - 1);
        var floorIndex = this.state.rnd.integerInRange(0, floors.length - 1);
        this.levelWidth = this.state.rnd.integerInRange(Game.GAME_WIDTH / this.tileWidth*1.5, Game.GAME_WIDTH / this.tileWidth * 2.5);
        this.levelHeight = this.state.rnd.integerInRange(Game.GAME_HEIGHT / this.tileHeight*1.5, Game.GAME_HEIGHT / this.tileHeight * 2.5);

        var wall = walls[wallIndex];
        var floor = floors[floorIndex];

        //Creates a new TileMap
        this.map = this.state.add.tilemap();
        //Adds two images as TileSets to our TileMap, which will allow us to place them
        this.map.addTilesetImage(floor, floor, this.tileWidth, this.tileHeight, null, null, 0);
        this.map.addTilesetImage(wall, wall, this.tileWidth, this.tileHeight, null, null, 1);
        //Creates a new layer, for the floor and sets the game's world size to the size of this layer
        this.floorLayer = this.map.create('floor', this.levelWidth, this.levelHeight, this.tileWidth, this.tileHeight);
        this.floorLayer.resizeWorld();
        //Create a layer for the walls.
        this.wallsLayer = this.map.createBlankLayer('walls', this.levelWidth, this.levelHeight, this.tileWidth, this.tileHeight);
        this.map.setCollisionByExclusion([], true, this.wallsLayer, false);

        //Create group for enemies
        this.enemies = this.state.add.group();
        this.enemies.enableBody = true;
        //Create group for collectibles
        this.collectibles = this.state.add.group();
        this.collectibles.enableBody = true;
        //Create group for our exit object
        this.exit = this.state.add.group();
        this.exit.enableBody = true;
        //Create walls group for collisoins
        this.walls = this.state.add.group();
        this.walls.enableBody = true;
        this.walls.physicsBodyType = Phaser.Physics.ARCADE;

        this.initialiseLevelData();

        this.generateLevel();
        this.populateTileMap();
    },

    /**
     *
     * @returns {number} - The width in pixels of the game world
     */
    getGameWidth: function(){
        return this.levelWidth*this.tileWidth;
    },

    /**
     *
     * @returns {number} - The height in pixels of the game world
     */
    getGameHeight: function(){
        return this.levelHeight*this.tileHeight;
    }
};

/**
 * Handy little function to create multidimensional arrays
 * @param length - Length of the array, add more length parameters to add more dimensions.
 * @returns {Array} - Your n-dimensional array.
 */
function createArray(length) {
    var arr = new Array(length || 0),
        i = length;

    if (arguments.length > 1) {
        var args = Array.prototype.slice.call(arguments, 1);
        while(i--) arr[length-1 - i] = createArray.apply(this, args);
    }

    return arr;
}