var express = require('express');
var UUID = require('uuid');
var port = process.env.PORT || 3000;
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var aws = require('aws-sdk');
aws.config = new aws.Config({
    accessKeyId: 'AKIAJZO4NRKYBNCQOMIA', secretAccessKey: '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', region: 'eu-west-1'
});
var dynamoDB = new aws.DynamoDB();
var clients = {};

app.use('/Scripts', express.static(__dirname + '/Scripts'));
app.use('/Images', express.static(__dirname + '/Images'));
app.use('/Sounds', express.static(__dirname + '/Sounds'));

app.get('/', function(req, res){
    res.sendFile(__dirname + '/index.html');
});

io.sockets.on('connection', function(socket) {
    socket.userid = UUID();
    clients[socket.userid] = {x: 0, y: 0};

    socket.emit('connected');
    console.log('Client ' + socket.userid + ' connected to server.');

    socket.on('disconnect', function () {
        console.log('Client ' + socket.userid + ' disconnected from server.');
        delete clients[socket.userid];
    });

    socket.on('playerPosition', function (data) {
        clients[socket.userid] = data;
    });

    socket.on('getHighscores', function(){
        var params = {
            TableName: "DungeonCrawler_Highscores",
            AttributesToGet: ['Score', 'PlayerName']
           // Limit: 10
        };

        dynamoDB.scan(params, function(err, data){
           if(err){
               console.log(err, err.stack);
               socket.emit('getHighscoresFailed');
           } else {
               socket.emit('getHighscores', data);
           }
        });
    });
    socket.on('sendNewScore', function(data){
        var params = {
          Item: {
              ID: {S: socket.userid},
              PlayerName: {S: data.name},
              Score: {N: data.score.toString()}
          },
            TableName: "DungeonCrawler_Highscores"
        };

        dynamoDB.putItem(params, function(err, data){
           if(err){
               console.log(err, err.stack);
               socket.emit('scorePostFailed');
           } else {
               socket.emit('scorePosted');
           }
        });
    });

    socket.on('getOtherPlayers', function () {
        var positions = [];
        for (var key in clients) {
            if (clients.hasOwnProperty(key)) {
                if (key != socket.userid) {
                    positions.push(clients[key]);
                }
            }
        }
        socket.emit('otherPlayers', positions);
    });
});
server.listen(port, function(){
    console.log('Listening on port: ' + port);
});